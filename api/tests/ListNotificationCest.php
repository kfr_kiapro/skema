<?php

class ListNotificationCest
{
    private $expectedTotal = 17;
    private $defaultPaginationPageSize = 10;
    private $brugerUuid = 'c010a94b-b076-4e75-ab07-01aba3064336';

    public function _before(ApiTester $I){}

    public function _after(ApiTester $I){}

    public function testCanList(ApiTester $I)
    {
        $I->haveHttpHeader('BrugerUuid', $this->brugerUuid);
        $I->sendGET('/notifikation');
        $I->seeResponseCodeIs(200);
        $I->seeResponseIsJson();
        $response = $I->grabResponse();
        $I->assertCount($this->expectedTotal, json_decode($response, true));
    }

    public function testCanListObjects(ApiTester $I)
    {
        $I->haveHttpHeader('BrugerUuid', $this->brugerUuid);
        $I->sendGET('/notifikation');
        $I->seeResponseCodeIs(200);
        $I->seeResponseIsJson();
        $response = json_decode($I->grabResponse());
        $I->assertTrue(is_array($response));
        $I->assertTrue(is_string($response[0]));

        $I->haveHttpHeader('BrugerUuid', $this->brugerUuid);
        $I->sendGET('/notifikation?showObjects=1');
        $I->seeResponseCodeIs(200);
        $I->seeResponseIsJson();
        $response = json_decode($I->grabResponse());
        $I->assertTrue(is_array($response));
        $I->assertTrue(is_object($response[0]));
    }

    public function testCanPaginate(ApiTester $I)
    {
        $I->haveHttpHeader('BrugerUuid', $this->brugerUuid);
        $I->sendGET('/notifikation?usePaging=1');
        $I->seeResponseCodeIs(200);
        $I->seeResponseIsJson();
        $response = json_decode($I->grabResponse(), true);
        $I->assertCount($this->defaultPaginationPageSize, $response['data']);
        $I->assertEquals($this->expectedTotal, $response['paging']['total']);
        $I->assertEquals(2, $response['paging']['number_of_pages']);
        $I->assertEquals(1, $response['paging']['page']);
        $I->assertEquals($this->defaultPaginationPageSize, $response['paging']['page_size']);

        $I->haveHttpHeader('BrugerUuid', $this->brugerUuid);
        $I->sendGET('/notifikation?usePaging=1&page=2');
        $I->seeResponseCodeIs(200);
        $I->seeResponseIsJson();
        $response = json_decode($I->grabResponse(), true);
        $I->assertCount(7, $response['data']);
        $I->assertEquals(2, $response['paging']['page']);

        $I->haveHttpHeader('BrugerUuid', $this->brugerUuid);
        $I->sendGET('/notifikation?usePaging=1&page=3');
        $I->seeResponseCodeIs(400);
        $I->seeResponseIsJson();

        $I->haveHttpHeader('BrugerUuid', $this->brugerUuid);
        $I->sendGET('/notifikation?usePaging=1&pageSize=5&page=4');
        $I->seeResponseCodeIs(200);
        $I->seeResponseIsJson();
        $response = json_decode($I->grabResponse(), true);
        $I->assertEquals(4, $response['paging']['number_of_pages']);
        $I->assertCount(2, $response['data']);
    }

    public function testCanApplySearchFilters(ApiTester $I)
    {
        $I->haveHttpHeader('BrugerUuid', $this->brugerUuid);
        $I->sendGET('/notifikation?brugervendtnoegle=test-list-1');
        $I->seeResponseCodeIs(200);
        $I->seeResponseIsJson();
        $response = json_decode($I->grabResponse(), true);
        $I->assertCount(13, $response);

        $I->haveHttpHeader('BrugerUuid', $this->brugerUuid);
        $I->sendGET('/notifikation?brugervendtnoegle=test*');
        $I->seeResponseCodeIs(200);
        $I->seeResponseIsJson();
        $response = json_decode($I->grabResponse(), true);
        $I->assertCount($this->expectedTotal, $response);

        $I->haveHttpHeader('BrugerUuid', $this->brugerUuid);
        $I->sendGET('/notifikation?brugervendtnoegle=test?');
        $I->seeResponseCodeIs(200);
        $I->seeResponseIsJson();
        $response = json_decode($I->grabResponse(), true);
        $I->assertCount($this->expectedTotal, $response);

        $I->haveHttpHeader('BrugerUuid', $this->brugerUuid);
        $I->sendGET('/notifikation?brugervendtnoegle=test%');
        $I->seeResponseCodeIs(200);
        $I->seeResponseIsJson();
        $response = json_decode($I->grabResponse(), true);
        $I->assertCount($this->expectedTotal, $response);

        $I->haveHttpHeader('BrugerUuid', $this->brugerUuid);
        $I->sendGET('/notifikation?brugervendtnoegle=test%&modtager_uuid=79ccd21b-143c-471d-b033-000000000013');
        $I->seeResponseCodeIs(200);
        $I->seeResponseIsJson();
        $response = json_decode($I->grabResponse(), true);
        $I->assertCount(1, $response);
        //NOTE OTHER ACCEPTED SEARCH FILTERS ARE:
        //            Notifikation::ATTRIBUTE_UUID,
        //            Notifikation::ATTRIBUTE_BRUGERVENDTNOEGLE,
        //            Notifikation::CONTEXT_AFSENDER_UUID,
        //            Notifikation::CONTEXT_AFSENDER_OBJEKTTYPE,
        //            Notifikation::CONTEXT_MOTAGER_UUID,
        //            Notifikation::ATTRIBUTE_BRUGER_UUID,
        //            Notifikation::ATTRIBUTE_LIFECYCLE
        //UUIDS cannot contain wildcards!!!!
    }

}
