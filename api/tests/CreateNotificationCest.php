<?php

use App\Objects\Notifikation;

class CreateNotificationCest
{
    private $brugerUuid = 'c010a94b-b076-4e75-ab07-01aba3064336';
    private $existingUuid = '567c6fbd-25e9-43e4-a4b7-ed13cf6812f7';

    public function testCanCreate(ApiTester $I)
    {
        $payload = $I->getPayload();
        $I->haveHttpHeader('BrugerUuid', $this->brugerUuid);
        $I->sendPOST('/notifikation', $payload);
        $I->seeResponseCodeIs(201);
        $I->seeResponseIsJson();
        $I->seeResponseContainsJson([
            Notifikation::FORMAT_KEY_OBJECT_TYPE => Notifikation::OBJECT_NAME,
            Notifikation::ATTRIBUTE_BRUGERVENDTNOEGLE => $payload[Notifikation::ATTRIBUTE_BRUGERVENDTNOEGLE],
            Notifikation::FORMAT_KEY_REGISTRATION => [
                Notifikation::ATTRIBUTE_BRUGER_UUID => $this->brugerUuid,
                Notifikation::ATTRIBUTE_LIFECYCLE => Notifikation::LIFECYCLE_OPERATION_CREATE
            ],
            Notifikation::FORMAT_KEY_EFFECT => [
                Notifikation::FORMAT_KEY_FROM => $payload[Notifikation::FORMAT_KEY_EFFECT][Notifikation::FORMAT_KEY_FROM],
                Notifikation::FORMAT_KEY_TO => $payload[Notifikation::FORMAT_KEY_EFFECT][Notifikation::FORMAT_KEY_TO],
                Notifikation::ATTRIBUTE_NOTE => $payload[Notifikation::FORMAT_KEY_EFFECT][Notifikation::ATTRIBUTE_NOTE],
            ],
        ]);
        $response = json_decode($I->grabResponse(), true);
        $I->assertNotEmpty($response[Notifikation::ATTRIBUTE_UUID]);
        $I->assertNotEmpty($response[Notifikation::FORMAT_KEY_REGISTRATION][Notifikation::ATTRIBUTE_TIMESTAMP]);
    }

    public function testForErrorOnUniqueConstraint(ApiTester $I)
    {
        $payload = $I->getPayload();
        $I->haveHttpHeader('BrugerUuid', $this->brugerUuid);
        $I->sendPOST('/notifikation', $payload);
        $I->seeResponseCodeIs(201);
        $I->seeResponseIsJson();

        $I->haveHttpHeader('BrugerUuid', $this->brugerUuid);
        $I->sendPOST('/notifikation', $payload);
        $I->seeResponseCodeIs(409);
        $I->seeResponseIsJson();
    }

    public function testForErrorOnExistingUuidIfPresent(ApiTester $I)
    {
        $payload = $I->getPayload();
        $payload[Notifikation::ATTRIBUTE_UUID] = $this->existingUuid;
        $I->haveHttpHeader('BrugerUuid', $this->brugerUuid);
        $I->sendPOST('/notifikation', $payload);
        $I->seeResponseCodeIs(409);
        $I->seeResponseIsJson();
    }

    public function testDoesApplyJsonSchemaValidation(ApiTester $I)
    {
        $payload = $I->getPayload();
        unset($payload[Notifikation::FORMAT_KEY_EFFECT]);
        $I->haveHttpHeader('BrugerUuid', $this->brugerUuid);
        $I->sendPOST('/notifikation', $payload);
        $I->seeResponseCodeIs(400);
        $I->seeResponseIsJson();
    }
}
