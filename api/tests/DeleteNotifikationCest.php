<?php


use App\Objects\Notifikation;

class DeleteNotifikationCest
{
    private $brugerUuid = 'c010a94b-b076-4e75-ab07-01aba3064123';
    private $existingUuid = '567c6fbd-25e9-43e4-a4b7-ed13cf6812f7';

    public function testCanDelete(ApiTester $I)
    {
        $I->haveHttpHeader('BrugerUuid', $this->brugerUuid);
        $I->sendDELETE('/notifikation/' . $this->existingUuid);
        $I->seeResponseCodeIs(200);
        $I->seeResponseIsJson();
        $I->seeResponseContainsJson([
            Notifikation::FORMAT_KEY_OBJECT_TYPE => Notifikation::OBJECT_NAME,
            Notifikation::FORMAT_KEY_REGISTRATION => [
                Notifikation::ATTRIBUTE_LIFECYCLE => Notifikation::LIFECYCLE_OPERATION_DELETE
            ],
        ]);
        $response = json_decode($I->grabResponse(), true);
    }
}
