<?php

use App\Objects\Notifikation;

class ImportNotifikationCest
{
    private $brugerUuid = 'c010a94b-b076-4e75-ab07-01aba3064336';
    private $newUuid = '567c6fbd-25e9-43e4-a4b7-123456789123';
    private $existingUuid = '4ac1482e-1e3e-4872-92fc-730ce52a1d00';


    public function _before(ApiTester $I){}

    public function _after(ApiTester $I){}

    public function testCanImportCreateWithUuid(ApiTester $I)
    {
        $payload = $I->getPayload();
        $payload[Notifikation::ATTRIBUTE_UUID] = $this->newUuid;
        $payload[Notifikation::ATTRIBUTE_BRUGERVENDTNOEGLE] = 'test-import';
        $I->haveHttpHeader('BrugerUuid', $this->brugerUuid);
        $I->sendPOST('/notifikation/import', $payload);
        $I->seeResponseCodeIs(201);
        $I->seeResponseIsJson();
        $I->seeResponseContainsJson([
            Notifikation::ATTRIBUTE_UUID => $this->newUuid,
            Notifikation::FORMAT_KEY_REGISTRATION => [
                Notifikation::ATTRIBUTE_BRUGER_UUID => $this->brugerUuid,
                Notifikation::ATTRIBUTE_LIFECYCLE => Notifikation::LIFECYCLE_OPERATION_IMPORT
            ],
        ]);
    }

    public function testCanImportUpdateWithUuid(ApiTester $I)
    {
        $payload = $I->getPayload();
        $payload[Notifikation::ATTRIBUTE_UUID] = $this->existingUuid;
        $payload[Notifikation::ATTRIBUTE_BRUGERVENDTNOEGLE] = 'test-update';
        $I->haveHttpHeader('BrugerUuid', $this->brugerUuid);
        $I->sendPOST('/notifikation/import', $payload);
        $I->seeResponseCodeIs(200);
        $I->seeResponseIsJson();
        $I->seeResponseContainsJson([
            Notifikation::ATTRIBUTE_UUID => $this->existingUuid,
            Notifikation::FORMAT_KEY_REGISTRATION => [
                Notifikation::ATTRIBUTE_BRUGER_UUID => $this->brugerUuid,
                Notifikation::ATTRIBUTE_LIFECYCLE => Notifikation::LIFECYCLE_OPERATION_IMPORT
            ],
        ]);
    }

    public function testDoesApplyJsonSchemaValidation(ApiTester $I)
    {
        $payload = $I->getPayload();
        unset($payload[Notifikation::FORMAT_KEY_EFFECT]);
        $I->haveHttpHeader('BrugerUuid', $this->brugerUuid);
        $I->sendPOST('/notifikation', $payload);
        $I->seeResponseCodeIs(400);
        $I->seeResponseIsJson();
    }
}
