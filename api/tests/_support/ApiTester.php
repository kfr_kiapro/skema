<?php

use App\Objects\Notifikation;

/**
 * Inherited Methods
 * @method void wantToTest($text)
 * @method void wantTo($text)
 * @method void execute($callable)
 * @method void expectTo($prediction)
 * @method void expect($prediction)
 * @method void amGoingTo($argumentation)
 * @method void am($role)
 * @method void lookForwardTo($achieveValue)
 * @method void comment($description)
 * @method \Codeception\Lib\Friend haveFriend($name, $actorClass = NULL)
 *
 * @SuppressWarnings(PHPMD)
*/
class ApiTester extends \Codeception\Actor
{
    use _generated\ApiTesterActions;

   /**
    * Define custom actions here
    */
    public function getPayload(): array
    {
        $notifikationsType = $this->getRelationArray(Notifikation::RELATION_NAME_NOTIFIKATIONSTYPER);
        $afsender = $this->getRelationArray('it-system');
        $modtager = $this->getRelationArray('person');
        return [
            Notifikation::FORMAT_KEY_OBJECT_TYPE => Notifikation::OBJECT_NAME,
            Notifikation::ATTRIBUTE_BRUGERVENDTNOEGLE => 'test-345',
            Notifikation::FORMAT_KEY_EFFECT => [
                Notifikation::ATTRIBUTE_NOTE => 'En note til virkningen',
                Notifikation::FORMAT_KEY_FROM => '2018-01-01 08:40:00',
                Notifikation::FORMAT_KEY_TO => '2018-12-31 08:40:00',
            ],
            Notifikation::OBJECT_NAME => [
                Notifikation::ATTRIBUTE_HEADER => 'Overskrift',
                Notifikation::ATTRIBUTE_BODY => 'Krop'
            ],
            Notifikation::FORMAT_KEY_DELIVERY => [
                Notifikation::ATTRIBUTE_SENT_TIMESTAMP => null,
                Notifikation::ATTRIBUTE_SEEN_TIMESTAMP => null,
                Notifikation::ATTRIBUTE_DELIVERED_TIMESTAMP => null
            ],
            Notifikation::FORMAT_KEY_ACTIONS => [
                Notifikation::ATTRIBUTE_UUID =>  "789f4dc4-cecb-4b9e-a976-459e423ed01c",
                Notifikation::ATTRIBUTE_ACTION => "Skal senest udføres",
                Notifikation::ATTRIBUTE_ACTION_TIMESTAMP =>  "2018-12-31 16:00:00",
                Notifikation::ATTRIBUTE_ACTION_LINK => "http://carex.dk/etandetendpoint",
                Notifikation::FORMAT_KEY_PARAMETERS => [
                    Notifikation::ATTRIBUTE_PARAMETER_NAME => "svaret på alt",
                    Notifikation::ATTRIBUTE_PARAMETER_VALUE => "42"
                ]
            ],
            Notifikation::FORMAT_KEY_CHANNELS => [
                Notifikation::ATTRIBUTE_CHANNEL_PRIMARY => 'sms',
                Notifikation::ATTRIBUTE_CHANNEL_SECONDARY => [
                    'email',
                    'push'
                ]
            ],
            Notifikation::FORMAT_KEY_RELATIONS => [
                Notifikation::RELATION_NAME_NOTIFIKATIONSTYPER => [$notifikationsType],
                Notifikation::RELATION_NAME_AFSENDER => [$afsender],
                Notifikation::RELATION_NAME_MODTAGER => [$modtager]
            ],
        ];
    }

    public function getRelationArray($relObjType, $relUuid = '3923cce9-4742-444c-ba03-4683c18a80a1', $relUri = '123', $note = 'en lille note'): array
    {
        return [
            Notifikation::ATTRIBUTE_RELATION_REL_OBJEKTTYPE => $relObjType,
            Notifikation::ATTRIBUTE_RELATION_REL_UUID => $relUuid,
            Notifikation::ATTRIBUTE_RELATION_REL_URI => $relUri,
            Notifikation::ATTRIBUTE_NOTE => $note
        ];
    }

    public function getRegistration($brugerUuid): array
    {
        return [
            Notifikation::ATTRIBUTE_LIFECYCLE => Notifikation::LIFECYCLE_OPERATION_CREATE,
            Notifikation::ATTRIBUTE_BRUGER_UUID => $brugerUuid,
            Notifikation::ATTRIBUTE_TIMESTAMP => '1970-01-01 18:00:00',
        ];
    }
}
