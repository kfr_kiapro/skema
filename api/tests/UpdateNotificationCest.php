<?php

use App\Objects\Notifikation;

class UpdateNotificationCest
{
    private $createBrugerUuid = 'c010a94b-b076-4e75-ab07-01aba3064336';
    private $brugerUuid = 'c010a94b-b076-4e75-ab07-01aba3064123';
    private $existingUuid = 'd997b51a-b957-4f46-888c-015dbbbed6de';
    private $brugervendtnoegleExistsUuid = 'ac397edd-95fc-4f5f-a7f5-e48f403706a9';
    private $contextModtagerUuid = '79ccd21b-143c-471d-b033-97cbb55ed790';
    private $contextAfsenderUuid = '79ccd21b-143c-471d-b033-97cbb55ed790';
    private $contextExistsUuid = '4ac1482e-1e3e-4872-92fc-730ce52a1d00';
    private $contextExistsModtagerUuid = '79ccd21b-143c-471d-b033-97cbb55ed123';
    private $contextExistsAfsenderUuid = '79ccd21b-143c-471d-b033-97cbb55ed123';

    public function testCanUpdate(ApiTester $I)
    {
        $payload = $I->getPayload();
        $payload[Notifikation::ATTRIBUTE_UUID] = $this->existingUuid;
        $payload[Notifikation::ATTRIBUTE_BRUGERVENDTNOEGLE] = 'test-update';
        $payload[Notifikation::FORMAT_KEY_REGISTRATION] = $I->getRegistration($this->createBrugerUuid);
        $I->haveHttpHeader('BrugerUuid', $this->brugerUuid);
        $I->sendPUT('/notifikation/' . $this->existingUuid, $payload);
        $I->seeResponseCodeIs(200);
        $I->seeResponseIsJson();
        $I->seeResponseContainsJson([
            Notifikation::FORMAT_KEY_OBJECT_TYPE => Notifikation::OBJECT_NAME,
            Notifikation::ATTRIBUTE_BRUGERVENDTNOEGLE => $payload[Notifikation::ATTRIBUTE_BRUGERVENDTNOEGLE],
            Notifikation::FORMAT_KEY_REGISTRATION => [
                Notifikation::ATTRIBUTE_BRUGER_UUID => $this->brugerUuid,
                Notifikation::ATTRIBUTE_LIFECYCLE => Notifikation::LIFECYCLE_OPERATION_UPDATE
            ],
        ]);
    }

    public function testForErrorOnUpdateBrugervendtnoegleAlreadyExistsForAnotherEntry(ApiTester $I)
    {
        $payload = $I->getPayload();
        $payload[Notifikation::ATTRIBUTE_UUID] = $this->existingUuid;
        $payload[Notifikation::ATTRIBUTE_BRUGERVENDTNOEGLE] = 'test-update-changed';
        $payload[Notifikation::FORMAT_KEY_REGISTRATION] = $I->getRegistration($this->createBrugerUuid);
        $payload[Notifikation::FORMAT_KEY_RELATIONS][Notifikation::RELATION_NAME_MODTAGER][0][Notifikation::ATTRIBUTE_RELATION_REL_UUID]
            = $this->contextModtagerUuid;
        $payload[Notifikation::FORMAT_KEY_RELATIONS][Notifikation::RELATION_NAME_AFSENDER][0][Notifikation::ATTRIBUTE_RELATION_REL_UUID]
            = $this->contextAfsenderUuid;
        $I->haveHttpHeader('BrugerUuid', $this->brugerUuid);
        $I->sendPUT('/notifikation/' . $this->existingUuid, $payload);
        $I->seeResponseCodeIs(409);
        $I->seeResponseIsJson();
        $I->seeResponseContainsJson([
            'uuid' => [$this->brugervendtnoegleExistsUuid]
        ]);
    }

    public function testForErrorOnUpdateContextAlreadyExistsForAnotherEntry(ApiTester $I)
    {
        $payload = $I->getPayload();
        $payload[Notifikation::ATTRIBUTE_UUID] = $this->existingUuid;
        $payload[Notifikation::ATTRIBUTE_BRUGERVENDTNOEGLE] = 'test-update';
        $payload[Notifikation::FORMAT_KEY_REGISTRATION] = $I->getRegistration($this->createBrugerUuid);
        $payload[Notifikation::FORMAT_KEY_RELATIONS][Notifikation::RELATION_NAME_MODTAGER][0][Notifikation::ATTRIBUTE_RELATION_REL_UUID]
            = $this->contextExistsModtagerUuid;
        $payload[Notifikation::FORMAT_KEY_RELATIONS][Notifikation::RELATION_NAME_AFSENDER][0][Notifikation::ATTRIBUTE_RELATION_REL_UUID]
            = $this->contextExistsAfsenderUuid;
        $I->haveHttpHeader('BrugerUuid', $this->brugerUuid);
        $I->sendPUT('/notifikation/' . $this->existingUuid, $payload);
        $I->seeResponseCodeIs(409);
        $I->seeResponseIsJson();
        $I->seeResponseContainsJson([
            'uuid' => [$this->contextExistsUuid]
        ]);
    }

    public function testForErrorOnPathUuidJsonUuidMismatch(ApiTester $I)
    {
        $payload = $I->getPayload();
        $payload[Notifikation::ATTRIBUTE_UUID] = $this->existingUuid;
        $I->haveHttpHeader('BrugerUuid', $this->brugerUuid);
        $I->sendPUT('/notifikation/' . $this->brugerUuid, $payload);
        $I->seeResponseCodeIs(409);
        $I->seeResponseIsJson();
    }

    public function testDoesApplyJsonSchemaValidation(ApiTester $I)
    {
        $payload = $I->getPayload();
        unset($payload[Notifikation::FORMAT_KEY_EFFECT]);
        $I->haveHttpHeader('BrugerUuid', $this->brugerUuid);
        $I->sendPOST('/notifikation', $payload);
        $I->seeResponseCodeIs(400);
        $I->seeResponseIsJson();
    }
}
