<?php

use App\Objects\Notifikation;

class AjourNotificationCest
{
    private $existingUuid = '4ac1482e-1e3e-4872-92fc-730ce52a1d00';
    private $brugerUuid = 'c010a94b-b076-4e75-ab07-01aba3064336';

    public function _before(ApiTester $I){}

    public function _after(ApiTester $I){}

    public function testCanAjourUpdateOnExisting(ApiTester $I) {
        $payload = $I->getPayload();
        $payload[Notifikation::ATTRIBUTE_BRUGERVENDTNOEGLE] = 'test-update';
        $afsenderRelation = $I->getRelationArray('it-system', '79ccd21b-143c-471d-b033-97cbb55ed123');
        $payload[Notifikation::FORMAT_KEY_RELATIONS][Notifikation::RELATION_NAME_AFSENDER] = [$afsenderRelation];
        $modtagerRelation = $I->getRelationArray('person', '79ccd21b-143c-471d-b033-97cbb55ed123');
        $payload[Notifikation::FORMAT_KEY_RELATIONS][Notifikation::RELATION_NAME_MODTAGER] = [$modtagerRelation];
        $I->haveHttpHeader('BrugerUuid', $this->brugerUuid);
        $I->sendPOST('/notifikation/ajour', $payload);
        $I->seeResponseCodeIs(200);
        $I->seeResponseIsJson();
        $I->seeResponseContainsJson([
            Notifikation::ATTRIBUTE_UUID => $this->existingUuid,
            Notifikation::FORMAT_KEY_REGISTRATION => [
                Notifikation::ATTRIBUTE_BRUGER_UUID => $this->brugerUuid,
                Notifikation::ATTRIBUTE_LIFECYCLE => Notifikation::LIFECYCLE_OPERATION_AJOUR
            ],
            Notifikation::FORMAT_KEY_EFFECT => [
                Notifikation::ATTRIBUTE_NOTE => $payload[Notifikation::FORMAT_KEY_EFFECT][Notifikation::ATTRIBUTE_NOTE],
                Notifikation::FORMAT_KEY_FROM => $payload[Notifikation::FORMAT_KEY_EFFECT][Notifikation::FORMAT_KEY_FROM],
                Notifikation::FORMAT_KEY_TO => $payload[Notifikation::FORMAT_KEY_EFFECT][Notifikation::FORMAT_KEY_TO],
            ],
        ]);
    }

    public function testDoesApplyJsonSchemaValidation(ApiTester $I)
    {
        $payload = $I->getPayload();
        unset($payload[Notifikation::FORMAT_KEY_EFFECT]);
        $I->haveHttpHeader('BrugerUuid', $this->brugerUuid);
        $I->sendPOST('/notifikation', $payload);
        $I->seeResponseCodeIs(400);
        $I->seeResponseIsJson();
    }
}
