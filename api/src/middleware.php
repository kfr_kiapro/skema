<?php
// Application middleware
// e.g: $app->add(new \Slim\Csrf\Guard);

$app->add(new App\Middleware\BrugerUuidMiddleware);
$app->add(new App\Middleware\AuthorizationMiddleware);
$app->add(new App\Middleware\CorsMiddleware);