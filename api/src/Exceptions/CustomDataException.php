<?php

namespace App\Exceptions;

use Exception, Throwable;

class CustomDataException extends Exception
{
    private $_data;

    public function __construct(string $message = "", int $code = 0, array $data = [], Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
        $this->_data = $data;
    }

    public function getData() {
        return $this->_data;
    }
}