<?php

namespace App\Exceptions;

class ApiException extends CustomDataException {
    const MISSING_BRUGER_UUID_HEADER = 1; //vi vil altid gerne have en bruger
    const MISSING_MANDATORY_INPUT = 2; //påkrævet input
    const CONSTRAINT_VIOLATION = 3; //constraints som vi tjekker på
    const DUPLICATE_CONFLICT = 4; //duplicat tjek fejler, typisk brugervendtnoegle og context
    const UNEXPECTED_STATE = 5; //tilstand er ikke som forventet for pågældende handling
    const NOT_FOUND = 6; //resource ikke fundet
    const NOT_ALLOWED = 7; //handling ikke tilladt, eks. man kan ikke aktivere en skabelon
    const UNAUTHORIZED = 8; //selvforklarende
    const DATABASE_ERROR = 9;
    const VALIDATION_VIOLATION = 10;
    const DISAMBIGUITY = 11;
}