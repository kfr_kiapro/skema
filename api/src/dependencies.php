<?php
// DIC configuration

use App\Exceptions\ApiException;

$container = $app->getContainer();

// monolog
$container['logger'] = function ($c) {
    $settings = $c->get('settings')['logger'];
    $logger = new Monolog\Logger($settings['name']);
    $logger->pushProcessor(new Monolog\Processor\UidProcessor());
    $logger->pushHandler(new Monolog\Handler\StreamHandler($settings['path'], $settings['level']));
    return $logger;
};

$container['errorHandler'] = function ($container) {
    return function ($request, $response, Exception $exception) use ($container) {
        $httpResponseCode = 500;
        $responseArray = ['error' => 'Internal Server Error'];
        switch (get_class($exception)) {
            case 'App\Exceptions\ApiException':
                if ($exception->getCode() === ApiException::MISSING_BRUGER_UUID_HEADER) {
                    $httpResponseCode = 400;
                }
                if (
                    $exception->getCode() === ApiException::MISSING_MANDATORY_INPUT ||
                    $exception->getCode() === ApiException::CONSTRAINT_VIOLATION ||
                    $exception->getCode() === ApiException::UNEXPECTED_STATE
                ) {
                    $httpResponseCode = 400;
                    $responseArray['data'] = $exception->getData();
                }
                if ($exception->getCode() === ApiException::VALIDATION_VIOLATION) {
                    $httpResponseCode = 400;
                    $responseArray['data'] = $exception->getData()['errors'];
                }
                if ($exception->getCode() === ApiException::UNAUTHORIZED) {
                    $httpResponseCode = 400;
                }
                if (
                    $exception->getCode() === ApiException::DUPLICATE_CONFLICT ||
                    $exception->getCode() === ApiException::DISAMBIGUITY
                ) {
                    $httpResponseCode = 409;
                    $responseArray['uuid'] = $exception->getData();
                }
                if (
                    $exception->getCode() === ApiException::NOT_FOUND
                ) {
                    $httpResponseCode = 404;
                }
                if (
                    $exception->getCode() === ApiException::NOT_ALLOWED
                ) {
                    $httpResponseCode = 405;
                }
                if ($exception->getCode() === ApiException::DATABASE_ERROR) {
                    $responseArray['data'] = $exception->getData();
                }
                $responseArray['error'] = $exception->getMessage();
                break;
            default:
                $responseArray['error'] = $exception->getMessage();
                break;
        }
        return $container['response']
            ->withStatus($httpResponseCode)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode($responseArray));
    };
};