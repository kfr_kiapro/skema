<?php

namespace App\Database;

use PDO, PDOException;

/**
 * Class DatabaseConnection
 *
 * this is a singleton implementation for database connection
 */
final class DatabaseConnection
{
    //private constructor means static initialisation only
    private function __construct(){}

    public static function getConnection(): PDO
    {
        static $connection = null;
        if ($connection === null) {
            $db_host = getenv("DB_HOST");
            $db_port = getenv("DB_PORT");
            $db_name = getenv("POSTGRES_DB");
            $db_username = getenv("POSTGRES_USER");
            $db_password = getenv("POSTGRES_PASSWORD");
            try{
                $connection = new PDO("pgsql:host=" . $db_host . ";port=" . $db_port . ";dbname=" . $db_name, $db_username, $db_password);
                $connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            } catch(PDOException $exception) {
                echo "Connection error: " . $exception->getMessage();
            }
        }
        return $connection;
    }
}