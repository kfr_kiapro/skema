<?php

namespace App\Database;

use App\Exceptions\ApiException;
use App\Objects\AbstractBaseObject;
use Exception;
use PDO, PDOStatement;

class DatabaseObject
{
    const DEFAULT_PAGE_SIZE = 10;

    private $dbConnection;

    public function __construct()
    {
        $this->dbConnection = DatabaseConnection::getConnection();
    }

    public function getTimestamp()
    {
        $query = "SELECT now() AT TIME ZONE 'Europe/Copenhagen'";
        $timestamp = $this->executeQuery($query)->fetchColumn();
        return substr($timestamp, 0, strpos($timestamp, '.'));
    }

    public function getUuid()
    {
        $query = "SELECT uuid_generate_v4()";
        return $this->executeQuery($query)->fetchColumn();
    }

    public function insertQuery(string $tableName, array $parameters): bool
    {
        list($fieldBindings, $bindingValues) = $this->extractQueryElements($parameters);
        $query = 'INSERT INTO ' .
            $tableName .
            '(' . implode(',',array_keys($fieldBindings)) . ') VALUES (' .
            implode(',',array_values($fieldBindings)) . ')';
        $statement = $this->executeQuery($query, $bindingValues);
        return ($statement->rowCount() === 1);
    }

    public function updateQuery(string $tableName, array $parameters, array $criteria): bool
    {
        if (!empty($parameters)) {
            list($fieldBindings, $bindingValues) = $this->extractQueryElements($parameters);
            list($criteriaBindings, $criteriaBindingValues) = $this->extractQueryElements($criteria);
            $query = 'UPDATE ' . $tableName . ' SET ';
            $bindings = [];
            foreach($fieldBindings as $field => $binding) {
                $bindings[] = $field . '=' . $binding;
            }
            $query .= implode(', ', $bindings);
            if (!empty($criteria)) {
                $where = [];
                foreach($criteriaBindings as $key => $value) {
                    //simple form, could also include operator, but right now we use =
                    if (isset($fieldBindings[$key])) {
                        //if keys appear both in $parameters and $criteria we need to give them another binding
                        $bindingValue = $criteriaBindingValues[$value];
                        unset($criteriaBindingValues[$value]);
                        $value = $value . '_1';
                        $criteriaBindingValues[$value] = $bindingValue;
                    }
                    $where[] = $key . '=' . $value;
                }
                $query .= ' WHERE ' . implode(' AND ', $where);
                $bindingValues = array_merge($bindingValues, $criteriaBindingValues);
            }
            $statement = $this->executeQuery($query, $bindingValues);
            return ($statement->rowCount() === 1);
        }
        return false;
    }

    public function selectQuery(string $tableName, array $parameters): array
    {
        list($fieldBindings, $bindingValues, $stringClauses) = $this->extractQueryElements($parameters);
        $query = 'SELECT * FROM ' . $tableName;
        if (!empty($parameters)) {
            $query .= ' WHERE ';
            $bindings = [];
            foreach($fieldBindings as $field => $binding) {
                $bindings[] = $field . '=' . $binding;
            }
            $query .= implode(' AND ', $bindings);
        }
        if (!empty($stringClauses)) {
            foreach($stringClauses as $index => $clause) {
                if (!empty($fieldBindings) || $index > 0) {
                    $query .= ' AND ' . $clause;
                }  else {
                    $query .= $clause;
                }
            }
        }
        $statement = $this->executeQuery($query, $bindingValues);
        return $this->extractRows($statement);
    }

    public function updateTransaction(string $tableName, array $parameters, string $timestamp): void
    {
        try {
            $this->dbConnection->beginTransaction();
            //get latest
            $criteria = [
                'uuid' => $parameters['uuid'],
                'is_latest' => 'true'
            ];
            $latest = $this->selectQuery($tableName, $criteria)[0];
            $registreringFrom = substr($latest['registrering'], 0, strpos($latest['registrering'], ','));
            $registrering = $registreringFrom . ',' . $timestamp . ')';
            //update latest
            $dataToUpdate = [
                'is_latest' => 'false',
                'registrering' => $registrering
            ];
            $this->updateQuery($tableName, $dataToUpdate, $criteria);
            //insert new one
            $this->insertQuery($tableName, $parameters);
            $this->dbConnection->commit();
        } catch (Exception $e) {
            $this->dbConnection->rollBack();
            throw new ApiException('Error during update transaction', ApiException::DATABASE_ERROR,
                [
                    'original_code' => $e->getCode(),
                    'original_error' => $e->getMessage()
                ]
            );
        }
    }

    public function search(string $dbTable, array $parameters, bool $showObjects, $usePaging = false, $paging = []): array
    {
        $results = [];
        if ($usePaging) {
            $total =  $this->searchQueryCount($dbTable, $parameters);
            $pageSize = ($paging['pageSize'] ?? self::DEFAULT_PAGE_SIZE);
            $numberOfPages = ceil($total / $pageSize);
            $page = $paging['page'] ?? 1;
            $results['paging'] = ['total' => $total, 'page_size' => $pageSize, 'page' => $page, 'number_of_pages' => $numberOfPages];
            if ($page > $numberOfPages) {
                throw new ApiException('Page is greater than number_of_pages', ApiException::UNEXPECTED_STATE, $results);
            }
            $records = $this->searchQuery($dbTable, $parameters, $results['paging']);
            $results['data'] = $this->formatSearchResults($showObjects, $records);
        } else {
            $records = $this->searchQuery($dbTable, $parameters);
            $results = $this->formatSearchResults($showObjects, $records);
        }
        return $results;
    }

    public function readHistory(string $dbTable, string $uuid, array $parameters): array
    {
        $bindingValues = [':' . AbstractBaseObject::ATTRIBUTE_UUID => $uuid];
        $query = "SELECT data FROM {$dbTable} WHERE uuid=:uuid";
        if (!empty($parameters)) {
            if (isset($parameters[AbstractBaseObject::SEARCH_REGISTRERING_FRA]) && isset($parameters[AbstractBaseObject::SEARCH_REGISTRERING_TIL])) {
                $fra = $parameters[AbstractBaseObject::SEARCH_REGISTRERING_FRA];
                $til = $parameters[AbstractBaseObject::SEARCH_REGISTRERING_TIL];
                $bindingValues[':fra'] = $fra;
                $bindingValues[':til'] = $til;
                $query .= " AND registrering <@ tsrange(:fra::timestamp,:til::timestamp, '[]')";
            } else if (isset($parameters[AbstractBaseObject::SEARCH_REGISTRERING_FRA])) {
                $fra = $parameters[AbstractBaseObject::SEARCH_REGISTRERING_FRA];
                $bindingValues[':fra'] = $fra;
                $bindingValues[':til'] = $fra;
                $query .= " AND registrering &> tsrange(:fra::timestamp,:til::timestamp, '[]')";
            } else if (isset($parameters[AbstractBaseObject::SEARCH_REGISTRERING_TIL])) {
                $til = $parameters[AbstractBaseObject::SEARCH_REGISTRERING_TIL];
                $bindingValues[':fra'] = $til;
                $bindingValues[':til'] = $til;
                $query .= " AND registrering &< tsrange(:fra::timestamp,:til::timestamp, '[]')";
            }
        }
        $query .= " ORDER BY registrering DESC";
        $statement = $this->executeQuery($query, $bindingValues);
        $records = $this->extractRows($statement);
        $results = [];
        foreach ($records as $record) {
            $results[] = json_decode($record[AbstractBaseObject::OBJECT_DATA]);
        }
        return $results;
    }

    public function getExisting(string $dbTable, array $parameters, $disregardEffect = false): array
    {
        $query = "SELECT * FROM {$dbTable} WHERE ";
        $virkning_from = $parameters[AbstractBaseObject::FORMAT_KEY_FROM];
        unset($parameters[AbstractBaseObject::FORMAT_KEY_FROM]);
        $virkning_to = $parameters[AbstractBaseObject::FORMAT_KEY_TO];
        unset($parameters[AbstractBaseObject::FORMAT_KEY_TO]);
        $uuid = null;
        if (isset($parameters[AbstractBaseObject::ATTRIBUTE_UUID])) {
            $uuid = $parameters[AbstractBaseObject::ATTRIBUTE_UUID];
            unset($parameters[AbstractBaseObject::ATTRIBUTE_UUID]);
        }
        list($fieldBindings, $bindingValues) = $this->extractQueryElements($parameters);
        if (!empty($parameters)) {
            $bindings = [];
            foreach($fieldBindings as $field => $binding) {
                $bindings[] = $field . '=' . $binding;
            }
            $query .= implode(' AND ', $bindings);
        }
        if (!$disregardEffect) {
            $query .= " AND virkning && tsrange(:fra::timestamp,:til::timestamp, '[]')";
            $bindingValues[':fra'] = $virkning_from;
            $bindingValues[':til'] = $virkning_to;
        }
        if (isset($uuid)) {
            $query .= " AND uuid <> :uuid";
            $bindingValues[':uuid'] = $uuid;
        }
        $statement = $this->executeQuery($query, $bindingValues);
        return $this->extractRows($statement);
    }

    protected function executeQuery(string $query, array $bindingValues = []): PDOStatement
    {
        $statement = $this->dbConnection->prepare($query);
        $statement->execute($bindingValues);
        $error = $statement->errorInfo();
        return $statement;
    }

    protected function extractRows(PDOStatement $statement): array
    {
        $arr = [];
        while ($row = $statement->fetch(PDO::FETCH_ASSOC)) {
            extract($row);
            $tmp = [];
            foreach ($row as $key => $value) {
                $tmp[$key] = $value;
            }
            $arr[] = $tmp;

            //$keys = array_keys($tmp);
        }
        return $arr;
    }

    protected function getCount(PDOStatement $statement): int
    {
        return (int)$statement->fetchColumn();
    }

    protected function extractQueryElements(array $parameters): array
    {
        $fieldBindings = [];
        $bindingValues = [];
        $stringClauses = [];
        foreach($parameters as $field => $value) {
            $bind = ':' . $field;
            if(!is_null($value)) {
                $fieldBindings[$field] = $bind;
                $bindingValues[$bind] = $value;
            }
            else {
                $stringClauses[] = $field . ' IS NULL';
            }
        }
        return [$fieldBindings, $bindingValues, $stringClauses];
    }

    protected function searchQuery(string $tableName, array $parameters, array $pagination = []): array
    {
        list($fieldBindings, $bindingValues) = $this->extractQueryElements($parameters);
        $query = 'SELECT * FROM ' . $tableName;
        $query .= ' WHERE ';
        if (!empty($parameters)) {
            $bindings = [];
            foreach($fieldBindings as $field => $binding) {
                if($this->containsWildcards($bindingValues[$binding])) {
                    $bindings[] = $field . ' LIKE ' . $binding;
                } else {
                    $bindings[] = $field . '=' . $binding;
                }
            }
            $query .= implode(' AND ', $bindings);
            $query .= ' AND ';
        }
        $query .= "is_latest='true'";
        if (!empty($pagination)) {
            $limit = $pagination['page_size'];
            $offset = ($limit * $pagination['page']) - $limit;
            $query .= ' LIMIT ' . $limit . ' OFFSET ' . $offset;
        }
        $values = [];
        foreach ($bindingValues as $key => $value) {
            $value = $this->replaceWildcards($value);
            $values[$key] = $value;
        }
        $statement = $this->executeQuery($query, $values);
        return $this->extractRows($statement);
    }

    protected function searchQueryCount(string $tableName, array $parameters): int
    {
        list($fieldBindings, $bindingValues) = $this->extractQueryElements($parameters);
        $query = 'SELECT count(*) as total FROM ' . $tableName;
        $query .= ' WHERE ';
        if (!empty($parameters)) {
            $bindings = [];
            foreach($fieldBindings as $field => $binding) {
                if($this->containsWildcards($bindingValues[$binding])) {
                    $bindings[] = $field . ' LIKE ' . $binding;
                } else {
                    $bindings[] = $field . '=' . $binding;
                }
            }
            $query .= implode(' AND ', $bindings);
            $query .= ' AND ';
        }
        $query .= "is_latest='true'";
        $values = [];
        foreach ($bindingValues as $key => $value) {
            $value = $this->replaceWildcards($value);
            $values[$key] = $value;
        }
        $statement = $this->executeQuery($query, $values);
        return $this->getCount($statement);
    }

    private function containsWildcards(string $value): bool
    {
        return (
            strstr($value, '*') || strstr($value, '?') || strstr($value,'%')
        );
    }

    private function replaceWildcards(string $value): string
    {
        return str_replace('*', '%', str_replace('?', '%', $value));
    }

    protected function formatSearchResults(bool $showObjects, array $records): array
    {
        $results = [];
        foreach ($records as $record) {
            if ($showObjects) {
                $results[] = json_decode($record[AbstractBaseObject::OBJECT_DATA]);
            } else {
                $results[] = $record[AbstractBaseObject::ATTRIBUTE_UUID];
            }
        }
        return $results;
    }
}