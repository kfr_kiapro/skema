<?php

namespace App\Objects;

use App\Exceptions\ApiException;
use stdClass;

class Skema extends AbstractBaseObject
{
    //generel
    const OBJECT_NAME = 'skema';
    const OBJECT_INDHOLD = 'indhold';

    const ATTRIBUTE_INDHOLD = 'indhold';

    // relationer
    const RELATION_NAME_TILHOERER = "tilhoerer";

    //context:
    const CONTEXT_TILHOERER_UUID = 'tilhoerer_uuid';

    public function __construct(string $brugerUuid, string $json = null)
    {
        parent::__construct($brugerUuid, $json);
        $this->objectName = 'skema';
    }

    public function create(string $lifecycleOperation = self::LIFECYCLE_OPERATION_CREATE): stdClass
    {
        $existing = $this->getExisting();

        if (!empty($existing)) {
            $uuids = [];
            foreach ($existing as $item) {
                $uuids[] = $item[self::ATTRIBUTE_UUID];
            }
            throw new ApiException('Skema already exists', ApiException::DUPLICATE_CONFLICT, $uuids);
        }

        if (!isset($this->stdClassObject->{self::ATTRIBUTE_UUID})) {
            $this->stdClassObject->{self::ATTRIBUTE_UUID} = $this->databaseObject->getUuid();
        } else {
            if ($this->uuidExists()) {
                throw new ApiException('Uuid is already occupied by another object', ApiException::DUPLICATE_CONFLICT, $uuids = [$this->stdClassObject->{self::ATTRIBUTE_UUID}]);
            }
        }

        $timestamp = $this->databaseObject->getTimestamp();

        if (!$this->hasRegistration()) {
            $this->addRegistration($timestamp, $lifecycleOperation);
        } else {
            $this->updateRegistration($timestamp, $lifecycleOperation);
        }



        $dataToInsert = $this->getDataToInsert($lifecycleOperation, $timestamp);

        $this->databaseObject->insertQuery($this->objectName, $dataToInsert);
        return $this->getObject();
    }

    public function update(string $uuid, string $lifecycleOperation = self::LIFECYCLE_OPERATION_UPDATE): stdClass
    {
        if ($uuid !== $this->stdClassObject->uuid) {
            $data['url'] = $uuid;
            $data['payload'] = $this->stdClassObject->uuid;
            throw new ApiException('There is a mismatch between url provided uuid and uuid contained in payload', ApiException::DISAMBIGUITY, $data);
        }
        $existing = $this->getExisting(true);
        if (!empty($existing)) {
            $uuids = [];
            foreach ($existing as $item) {
                $uuids[] = $item[self::ATTRIBUTE_UUID];
            }
            $uuids['supplied'] = $this->stdClassObject->uuid;
            throw new ApiException('Notifikation for that context or brugervendtnoegle already exists', ApiException::DUPLICATE_CONFLICT, $uuids);
        }
        $timestamp = $this->databaseObject->getTimestamp();
        if (!$this->hasRegistration()) {
            $this->addRegistration($timestamp, $lifecycleOperation);
        } else {
            $this->updateRegistration($timestamp, $lifecycleOperation);
        }
        $dataToInsert = $this->getDataToInsert($lifecycleOperation, $timestamp);
        $this->databaseObject->updateTransaction($this->objectName, $dataToInsert, $timestamp);
        return $this->getObject();
    }

    public function read(string $uuid):string
    {
        $parameters = [
            self::ATTRIBUTE_UUID => $uuid,
            self::OBJECT_LATEST => 'true'
        ];
        $result = $this->databaseObject->selectQuery($this->objectName, $parameters)[0];
        return $result['data'];
    }

    public function delete(string $uuid): stdClass
    {
        $this->stdClassObject = json_decode($this->read($uuid));
        $timestamp = $this->databaseObject->getTimestamp();
        $this->updateRegistration($timestamp, self::LIFECYCLE_OPERATION_DELETE);
        $dataToInsert = $this->getDataToInsert(self::LIFECYCLE_OPERATION_DELETE, $timestamp);
        $this->databaseObject->updateTransaction($this->objectName, $dataToInsert, $timestamp);
        return $this->getObject();
    }

    public function search(array $parameters): array
    {
        $showObjects = false;
        if (isset($parameters['showObjects']) && $parameters['showObjects']) {
            $showObjects = true;
        }
        $usePaging = false;
        $paging = [];
        if (isset($parameters['usePaging']) && $parameters['usePaging']) {
            $usePaging = true;
            if (isset($parameters['page']) && is_numeric($parameters['page'])) {
                $paging['page'] = (int)$parameters['page'];
            }
            if (isset($parameters['pageSize']) && is_numeric($parameters['pageSize'])) {
                $paging['pageSize'] = (int)$parameters['pageSize'];
            }
        }
        $validSearchAttributes = [
            self::ATTRIBUTE_UUID,
            self::ATTRIBUTE_BRUGERVENDTNOEGLE,
            self::CONTEXT_AFSENDER_UUID,
            self::CONTEXT_AFSENDER_OBJEKTTYPE,
            self::CONTEXT_MOTAGER_UUID,
            self::ATTRIBUTE_BRUGER_UUID,
            self::ATTRIBUTE_LIFECYCLE
        ];
        $searchParams = [];
        foreach ($parameters as $attributeName => $value) {
            if (in_array($attributeName, $validSearchAttributes)) {
                $searchParams[$attributeName] = $value;
            }
        }
        return $this->databaseObject->search($this->objectName, $searchParams, $showObjects, $usePaging, $paging);
    }

    public function history(string $uuid, array $searchTerms): array
    {
        $validAttributes = [
            self::SEARCH_REGISTRERING_FRA,
            self::SEARCH_REGISTRERING_TIL
        ];
        $searchParams = [];
        foreach ($searchTerms as $attributeName => $value) {
            if (in_array($attributeName, $validAttributes)) {
                $searchParams[$attributeName] = $value;
            }
        }
        return $this->databaseObject->readHistory($this->objectName, $uuid, $searchParams);
    }

    public function import(int &$code): stdClass
    {
        if ($this->uuidExists()) {
            $code = 200;
            return $this->update($this->stdClassObject->uuid, self::LIFECYCLE_OPERATION_IMPORT);
        }
        $code = 201;
        return $this->create(self::LIFECYCLE_OPERATION_IMPORT);
    }

    public function ajour(int &$code): stdClass
    {
        if (isset($this->stdClassObject->uuid)) {
            return $this->import($code);
        }
        $existing = $this->getExisting(false, true);
        if (empty($existing)) {
            $code = 201;
            return $this->create(self::LIFECYCLE_OPERATION_AJOUR);
        } else {
            if (count($existing) === 1) {
                $this->stdClassObject->uuid = $existing[0][self::ATTRIBUTE_UUID];
                $code = 200;
                return $this->update($this->stdClassObject->uuid, self::LIFECYCLE_OPERATION_AJOUR);
            }
            throw new ApiException('Disambiguous result, cannot assign UUID to object', ApiException::DISAMBIGUITY);
        }
    }

    protected function getExisting($exceptUuid = false, $disregardEffect = false): array
    {
        $parameters = [];
        if ($exceptUuid) {
            $parameters[self::ATTRIBUTE_UUID] = $this->stdClassObject->{self::ATTRIBUTE_UUID};
        }

        $parameters[self::ATTRIBUTE_BRUGERVENDTNOEGLE] = $this->stdClassObject->{self::ATTRIBUTE_BRUGERVENDTNOEGLE};
        $parameters[self::FORMAT_KEY_FROM] = $this->stdClassObject->{self::FORMAT_KEY_EFFECT}->{self::FORMAT_KEY_FROM};
        $parameters[self::FORMAT_KEY_TO] = $this->stdClassObject->{self::FORMAT_KEY_EFFECT}->{self::FORMAT_KEY_TO};
        //context: tilhoerer_uuid
        $tilhoererRelation = $this->getRelation(self::RELATION_NAME_TILHOERER)[0];
        $parameters[self::CONTEXT_TILHOERER_UUID] = $tilhoererRelation->{self::ATTRIBUTE_RELATION_REL_UUID};
        $parameters[self::OBJECT_LATEST] = 'true';
        return $this->databaseObject->getExisting($this->objectName, $parameters, $disregardEffect);
    }

    protected function getDataToInsert(string $lifecycleOperation, string $timestamp): array
    {
        $effectFrom = $this->stdClassObject->{self::FORMAT_KEY_EFFECT}->{self::FORMAT_KEY_FROM};
        $effectTo = $this->stdClassObject->{self::FORMAT_KEY_EFFECT}->{self::FORMAT_KEY_TO};
        $tilhoererRelation = $this->getRelation(self::RELATION_NAME_TILHOERER)[0];

        $indhold = $this->stdClassObject->{self::ATTRIBUTE_INDHOLD};

        $tilhoererUuid = $tilhoererRelation->{self::ATTRIBUTE_RELATION_REL_UUID};
        $registrationRange = '[' . $timestamp . ',)';
        if ($lifecycleOperation === self::LIFECYCLE_OPERATION_DELETE) {
            $registrationRange = '[' . $timestamp . ',' . $timestamp . ']';
        }

        $jsonObject = json_encode($this->stdClassObject);
        return [
            self::ATTRIBUTE_UUID => $this->stdClassObject->{self::ATTRIBUTE_UUID},
            self::ATTRIBUTE_OBJEKTTYPE => $this->objectName,
            self::ATTRIBUTE_INDHOLD => $indhold,

            self::ATTRIBUTE_BRUGERVENDTNOEGLE => $this->stdClassObject->{self::ATTRIBUTE_BRUGERVENDTNOEGLE},
            self::FORMAT_KEY_REGISTRATION => $registrationRange,
            self::FORMAT_KEY_EFFECT => '[' . ((isset($effectFrom))?$effectFrom:"") . ',' . ((isset($effectTo))?$effectTo:"") . ')',
            self::ATTRIBUTE_BRUGER_UUID => $this->brugerUuid,
            self::ATTRIBUTE_LIFECYCLE => $lifecycleOperation,
            self::OBJECT_DATA => $jsonObject,
            self::OBJECT_LATEST => 'true',
            self::CONTEXT_TILHOERER_UUID => $tilhoererUuid
        ];
    }
}