<?php

namespace App\Objects;

use App\Database\DatabaseObject;
use App\Exceptions\ApiException;
use JsonSchema\Validator;
use stdClass;

abstract class AbstractBaseObject
{
    //generel
    const OBJECT_DATA = 'data';
    const OBJECT_LATEST = 'is_latest';

    //objekt
    const ATTRIBUTE_UUID = 'uuid';
    const ATTRIBUTE_OBJEKTTYPE = 'objekttype';
    const ATTRIBUTE_BRUGERVENDTNOEGLE = 'brugervendtnoegle';
    const ATTRIBUTE_NOTE = 'note';

    //livscyklus:
    const ATTRIBUTE_LIFECYCLE = 'livscyklus';
    const ATTRIBUTE_BRUGER_UUID = 'bruger_uuid';
    const ATTRIBUTE_TIMESTAMP = 'timestamp';

    //operationer:
    const LIFECYCLE_OPERATION_CREATE = 'create';
    const LIFECYCLE_OPERATION_UPDATE = 'update';
    const LIFECYCLE_OPERATION_DELETE = 'delete';
    const LIFECYCLE_OPERATION_IMPORT = 'import';
    const LIFECYCLE_OPERATION_AJOUR = 'ajour';

    //relationer
    const ATTRIBUTE_RELATION_UUID = 'uuid';
    const ATTRIBUTE_RELATION_RELATIONSNAVN = 'relationsnavn';
    const ATTRIBUTE_RELATION_REL_UUID = 'rel_uuid';
    const ATTRIBUTE_RELATION_REL_URI = 'rel_uri';
    const ATTRIBUTE_RELATION_REL_OBJEKTTYPE = 'rel_objekttype';
    const ATTRIBUTE_RELATION_NOTE = 'note';

    //formateringer
    const FORMAT_KEY_DELIVERY = 'levering';
    const FORMAT_KEY_RELATIONS = 'relationer';
    const FORMAT_KEY_OBJECT_TYPE = 'objekttype';
    const FORMAT_KEY_REGISTRATION = 'registrering';
    const FORMAT_KEY_FROM = 'fra';
    const FORMAT_KEY_TO = 'til';
    const FORMAT_KEY_EFFECT = 'virkning';

    //search ranges
    const SEARCH_REGISTRERING_FRA = 'registrering_fra';
    const SEARCH_REGISTRERING_TIL = 'registrering_til';

    public $stdClassObject;
    public $brugerUuid;
    public $databaseObject;
    public $objectName;

    public function __construct(string $brugerUuid, string $json = null)
    {
        if (isset($json)) {
            $this->stdClassObject = json_decode($json);
        }
        $this->brugerUuid = $brugerUuid;
        $this->databaseObject = new DatabaseObject();
        $this->objectName = 'abstract';
    }

    abstract public function create(string $lifecycleOperation = self::LIFECYCLE_OPERATION_CREATE): stdClass;
    abstract public function update(string $uuid, string $lifecycleOperation = self::LIFECYCLE_OPERATION_UPDATE): stdClass;
    abstract public function read(string $uuid): string;
    abstract public function delete(string $uuid): stdClass;
    abstract public function search(array $parameters): array;
    abstract public function history(string $uuid, array $searchTerms): array;
    abstract public function import(int &$code): stdClass;
    abstract public function ajour(int &$code): stdClass;
    abstract protected function getExisting($exceptUuid = false, $disregardEffect = false): array;
    abstract protected function getDataToInsert(string $lifecycleOperation, string $timestamp): array;

    public function validate(string $schemaFile): void
    {
        $validator = new Validator;
        $file = 'file://' . getcwd(). '/Schema/' . $schemaFile;
        $validator->validate($this->stdClassObject, (object)['$ref' => $file]);

        if (!$validator->isValid()) {
            $errors = [];
            foreach ($validator->getErrors() as $error) {
                $errors[] = sprintf("[%s] %s", $error['property'], $error['message']);
            }
            throw new ApiException("The provided JSON does not validate.", ApiException::VALIDATION_VIOLATION, ['errors' => $errors]);
        }
    }

    protected function getRelation(string $relationName): ?array
    {
        $relation = $this->stdClassObject->{self::FORMAT_KEY_RELATIONS};
        if(isset($this->stdClassObject->{self::FORMAT_KEY_RELATIONS}->$relationName)) {
            return $this->stdClassObject->{self::FORMAT_KEY_RELATIONS}->$relationName;
        }
        return null;
    }

    protected function updateRegistration(string $timestamp, string $lifecycleOperation): void
    {
        $registration = $this->stdClassObject->{self::FORMAT_KEY_REGISTRATION};
        $registration->{self::ATTRIBUTE_LIFECYCLE} = $lifecycleOperation;
        $registration->{self::ATTRIBUTE_BRUGER_UUID} = $this->brugerUuid;
        $registration->{self::ATTRIBUTE_TIMESTAMP} = $timestamp;
        $this->stdClassObject->{self::FORMAT_KEY_REGISTRATION} = $registration;
    }

    protected function addRegistration(string $timestamp, string $lifecycleOperation): void
    {
        //might come from db service
        $registration = new stdClass();
        $registration->{self::ATTRIBUTE_LIFECYCLE} = $lifecycleOperation;
        $registration->{self::ATTRIBUTE_BRUGER_UUID} = $this->brugerUuid;
        $registration->{self::ATTRIBUTE_TIMESTAMP} = $timestamp;
        $this->stdClassObject->{self::FORMAT_KEY_REGISTRATION} = $registration;
    }

    protected function getObject(): stdClass
    {
        return $this->stdClassObject;
    }

    protected function hasRegistration(): bool
    {
        return isset($this->stdClassObject->{self::FORMAT_KEY_REGISTRATION});
    }

    protected function uuidExists(): bool
    {
        $uuidExisting = $this->databaseObject->selectQuery(
            $this->objectName,
            [self::ATTRIBUTE_UUID => $this->stdClassObject->{self::ATTRIBUTE_UUID}]
        );
        return (!empty($uuidExisting));
    }
}