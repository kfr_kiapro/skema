<?php

namespace App\Middleware;

use App\Exceptions\ApiException;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

class BrugerUuidMiddleware
{
    public function __invoke(ServerRequestInterface $request, ResponseInterface $response, callable $next)
    {
        if (empty($request->getHeader('BrugerUuid'))) {
            throw new ApiException('Missing BrugerUuid header', ApiException::MISSING_BRUGER_UUID_HEADER);
        }
        return $next($request, $response);
    }
}