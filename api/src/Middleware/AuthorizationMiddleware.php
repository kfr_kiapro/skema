<?php

namespace App\Middleware;

use App\Exceptions\ApiException;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\ResponseInterface;

class AuthorizationMiddleware
{
    public function __invoke(ServerRequestInterface $request, ResponseInterface $response, callable $next)
    {
        $authorizationToken = $request->getHeader('Authorization');
        if (empty($authorizationToken) || $authorizationToken[0] !== getenv('AUTHORIZATION_TOKEN')) {
            throw new ApiException('You are not authorized to use this endpoint', ApiException::UNAUTHORIZED);
        }
        return $next($request, $response);
    }
}