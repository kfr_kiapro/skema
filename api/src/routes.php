<?php

use App\Objects\Skema;
use Slim\Http\Request;
use Slim\Http\Response;

//ajour
$app->post('/skema/ajour', function (Request $request, Response $response) {
    $data = $request->getBody();
    $brugerUuid = $request->getHeader('BrugerUuid')[0];
    $objektdefinition = $request->getHeader('objektdefinition')[0];
    $skema = new Skema($brugerUuid, $data);
    if($objektdefinition == true){
        $skema->validate('createSkema_od.json');
    } else {
        $skema->validate('createSkema.json');
    }
    $code = 200;
    $result = $skema->ajour($code);
    return $response->withJson($result, $code);
});

//import
$app->post('/skema/import', function (Request $request, Response $response) {
    $data = $request->getBody();
    $brugerUuid = $request->getHeader('BrugerUuid')[0];
    $objektdefinition = $request->getHeader('objektdefinition')[0];
    $skema = new Skema($brugerUuid, $data);
    if($objektdefinition == true){
        $skema->validate('updateSkema.json');
    } else {
        $skema->validate('updateSkema.json');
    }
    $code = 200;
    $result = $skema->import($code);
    return $response->withJson($result, $code);
});

//create
$app->post('/skema', function (Request $request, Response $response) {
    $data = $request->getBody();
    $brugerUuid = $request->getHeader('BrugerUuid')[0];
    $objektdefinition = $request->getHeader('objektdefinition')[0];
    $skema = new Skema($brugerUuid, $data);
    if($objektdefinition == true){
        $skema->validate('createSkema_od.json');
    } else {
        $skema->validate('createSkema.json');
    }
    return $response->withJson($skema->create(), 201);
});

//update
$app->put('/skema/{uuid:[a-fA-F0-9]{8}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{12}}',
    function (Request $request, Response $response, array $args) {
        $data = $request->getBody();
        $brugerUuid = $request->getHeader('BrugerUuid')[0];
        $objektdefinition = $request->getHeader('objektdefinition')[0];
        $skema = new Skema($brugerUuid, $data);
        if($objektdefinition == true){
            $skema->validate('updateSkema_od.json');
        } else {
            $skema->validate('updateSkema.json');
        }
        return $response->withJson($skema->update($args['uuid']), 200);
    }
);

//read
$app->get('/skema/{uuid:[a-fA-F0-9]{8}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{12}}',
    function (Request $request, Response $response, array $args) {
        $brugerUuid = $request->getHeader('BrugerUuid')[0];
        $skema = new Skema($brugerUuid);
        return $response
            ->write($skema->read($args['uuid']))
            ->withStatus(200)
            ->withHeader('Content-Type', 'application/json');
    }
);

//history
$app->get('/skema/{uuid:[a-fA-F0-9]{8}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{12}}/historik',
    function (Request $request, Response $response, array $args) {
        $searchTerms = $request->getParams();
        $brugerUuid = $request->getHeader('BrugerUuid')[0];
        $skema = new Skema($brugerUuid);
        return $response->withJson($skema->history($args['uuid'], $searchTerms), 200);
    }
);

//delete
$app->delete(
    '/skema/{uuid:[a-fA-F0-9]{8}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{12}}',
    function (Request $request, Response $response, array $args) {
        $brugerUuid = $request->getHeader('BrugerUuid')[0];
        $skema = new Skema($brugerUuid);
        return $response->withJson($skema->delete($args['uuid']), 200);
    }
);

//list (search)
$app->get('/skema', function (Request $request, Response $response) {
    $parameters = $request->getParams();
    $brugerUuid = $request->getHeader('BrugerUuid')[0];
    $skema = new Skema($brugerUuid);
    return $response->withJson($skema->search($parameters), 200);
});
