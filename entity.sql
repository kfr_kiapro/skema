CREATE EXTENSION IF NOT EXISTS "uuid-ossp";
CREATE EXTENSION IF NOT EXISTS "btree_gist";

DROP table skema;
DROP type lifecycle cascade;

CREATE TYPE lifecycle AS ENUM ('create', 'update', 'delete', 'import', 'ajour');

CREATE TABLE IF NOT EXISTS skema (
  uuid              uuid         not null,
  objekttype        varchar(255) not null,
  indhold           varchar(255) not null,
  brugervendtnoegle varchar(255) not null,
  registrering      tsrange      not null,
  virkning          tsrange      not null,
  livscyklus        lifecycle    not null,
  bruger_uuid       uuid         not null,
  data              jsonb        not null,
  is_latest         boolean      not null,
  -- add context specific fields
  tilhoerer_uuid    uuid         not null,
  EXCLUDE USING GIST (uuid WITH =, registrering WITH &&)
);
CREATE INDEX IF NOT EXISTS skema_index
  ON skema
  USING GIST (brugervendtnoegle, virkning, tilhoerer_uuid);

/* eksempeldata
INSERT INTO public.skema (uuid, brugervendtnoegle, registrering, virkning, livscyklus, bruger_uuid, data, is_latest, tilhoerer_uuid)
VALUES
('c832f6d2-655e-4d52-841c-22393c10ffb8',
 'od_organisation',
 '"2018-10-25 00:00:00","2018-10-25 00:00:00"',
 null,
 'create',
 '764be998-fa65-4c3d-86ef-40c4e2507742',
 '{"uuid": "c832f6d2-655e-4d52-841c-22393c10ffb8",
  "brugervendtnoegle": "od_organisation",

  "menunummer": "1",
  "forretningsobjektnavn": "Objektdefinition af organisation",
  "felter":
  {
    "uuid": {
      "uuid": "0c8ab909-ce10-4e63-afea-286b521444d3",
      "brugervendtnoegle": "uuid",
      "elementnummer": "1",
      "elementnavn": "id",
      "elementtype": "identifikation",
      "feltegenskaber": ["uuid"],
      "afgraensning": "",
      "relationer": {
        "sprog": [
          {
            "rel_objekttype": "klasse",
            "rel_uuid": "132f4dc4-cecb-4b9e-a976-459e423ed01c"
          }
        ],
        "vaerdi": [
        ]
      }
    }
  ,
    "fra": {
      "uuid": "8860aa7d-5bc4-4581-b8e8-16dc5eed2f63",
      "brugervendtnoegle": "fra",
      "elementnummer": "2",
      "elementnavn": "fra",
      "elementtype": "date",
      "feltegenskaber": ["virkning"],
      "afgraensning": [""],
      "vaerdi": []
    ,
      "sprog": [
        {
          "rel_objekttype": "klasse",
          "rel_uuid": "132f4dc4-cecb-4b9e-a976-459e423ed01c"
        }
      ]
    }
  ,
    "til": {
      "uuid": "b964aef2-c86a-47f5-876e-7b2dcb636942",
      "brugervendtnoegle": "til",
      "elementnummer": "4",
      "elementnavn": "til",
      "elementtype": "date",
      "feltegenskaber": ["virkning"],
      "afgraensning": [""],
      "vaerdi":[],
      "sprog": [
        {
          "rel_objekttype": "klasse",
          "rel_uuid": "132f4dc4-cecb-4b9e-a976-459e423ed01c"
        }
      ]
    }
  ,
    "note": {
      "uuid": "b964aef2-c86a-47f5-876e-7b2dcb636942",
      "brugervendtnoegle": "note",
      "elementnummer": "4",
      "elementnavn": "note",
      "elementtype": "text",
      "feltegenskaber": ["virkning"],
      "afgraensning": [""],
      "vaerdi":[]
    ,
      "sprog": [
        {
          "rel_objekttype": "klasse",
          "rel_uuid": "132f4dc4-cecb-4b9e-a976-459e423ed01c"
        }
      ]
    }
  ,
    "brugervendtnoegle": {
      "uuid": "7578f2ad-478a-4d2a-9be9-c7daafcfe81f",
      "brugervendtnoegle": "brugervendtnoegle",
      "elementnummer": "5",
      "elementnavn": "brugervendtnoegle",
      "kontekst": ["virksomhed"],
      "afgraensning": [""],
      "vaerdi":[],
      "sprog": [
        {
          "rel_objekttype": "klasse",
          "rel_uuid": "132f4dc4-cecb-4b9e-a976-459e423ed01c"
        }
      ]
    }
  ,
    "organisationnavn": {
      "uuid": "e5de4e14-effe-4a86-be04-a3c2a7071a23",
      "brugervendtnoegle": "organisationnavn",
      "elementnummer": "6",
      "elementnavn": "organisationnavn",
      "elementtype": "text",
      "feltegenskaber": ["attributter"],
      "afgraensning": [""],
      "vaerdi":[]
    ,
      "sprog": [
        {
          "rel_objekttype": "klasse",
          "rel_uuid": "132f4dc4-cecb-4b9e-a976-459e423ed01c"
        }
      ]
    }
  ,
    "virksomhedstype": {
      "uuid": "0c8ab909-ce10-4e63-afea-286b521444d3",
      "brugervendtnoegle": "virksomhedstype",
      "elementnummer": "21",
      "elementnavn": "virksomhedstype",
      "elementtype": "relation",
      "feltegenskaber": [""],
      "afgraensning": [""],
      "vaerdi":
      {
        "rel_objekttype": "klasse",
        "rel_uuid": "132f4dc4-cecb-4b9e-a976-459e423ed01c",
        "group": "1"
      }
    ,
      "sprog": [
        {
          "rel_objekttype": "klasse",
          "rel_uuid": "132f4dc4-cecb-4b9e-a976-459e423ed01c"
        }
      ]
    }
  ,
    "tilknyttedefunktioner": {
      "uuid": "88f07e4f-42a0-45a9-bea7-a2528c277518",
      "brugervendtnoegle": "tilknyttedefunktioner",
      "elementnummer": "52",
      "elementnavn": "tilknyttedefunktioner",
      "elementtype": "relationer",
      "feltegenskaber": [""],
      "afgraensning": [""],
      "vaerdi":
      {
        "rel_objekttype": "funktion"
      }
    ,
      "sprog": [
        {
          "rel_objekttype": "klasse",
          "rel_uuid": "132f4dc4-cecb-4b9e-a976-459e423ed01c"
        }
      ]
    },
    "tilknyttedebrugere": {
      "uuid": "4803c6c1-9b49-4023-9db0-7c3aaecdba71",
      "brugervendtnoegle": "tilknyttedebrugere",
      "elementnummer": "53",
      "elementnavn": "tilknyttedebrugere",
      "elementtype": "relationer",
      "feltegenskaber": [""],
      "afgraensning": [""],
      "vaerdi":
      {
        "rel_objekttype": "bruger"
      }
    ,
      "sprog": [
        {
          "rel_objekttype": "klasse",
          "rel_uuid": "132f4dc4-cecb-4b9e-a976-459e423ed01c"
        }
      ]
    }
  }
,
  "relationer": {
    "ejer": [
      {
        "rel_objekttype": "organisation",
        "rel_uuid": "132f4dc4-cecb-4b9e-a976-459e423ed01c"
      }
    ],
    "tilhoerer": [
      {
        "rel_objekttype": "organisation",
        "rel_uuid": "132f4dc4-cecb-4b9e-a976-459e423ed01c"
      }
    ],
    "ansvarlig": [
      {
        "rel_objekttype": "organisation",
        "rel_uuid": "132f4dc4-cecb-4b9e-a976-459e423ed01c"
      }
    ],
    "redaktoerer": [
      {
        "rel_objekttype": "bruger",
        "rel_uuid": "79ccd21b-143c-471d-b033-97cbb55ed790"
      }
    ]
  },
  "virkning": {
    "fra": "2018-10-01 16:00:00",
    "til": null,
    "note": null
  },
  "registrering": {
    "livscyklus": "create",
    "bruger_uuid": "9b770978-2b12-46f3-8fcd-3944e68845ec",
    "timestamp": "2018-09-12 14:31:05"
  }
}',
 true,
 '59aa2f87-d09e-4fd7-9837-d3342082b35a');
*/
