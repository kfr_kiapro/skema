# Skema-microservice #

## Installation af microservice ##
In order to run the microservice locally on Windows for development, create a new phpstorm project somewhere outside of
your homedirectory, e.g. d:\\. Then using phpstorm VCS->Git->Clone... to clone the repository.
The repo resides at https://bitbucket.org/KIAtec/notifikation/src/master/
 
### Running Docker under Windows ######
Fetch and install Docker for Windows [here](https://docs.docker.com/docker-for-windows/install/), this
requires having setup a user account with docker.
Next it is necessary to share the d:\\ drive with docker, so that we can map the directory *api* in docker.
This [guide](http://peterjohnlightfoot.com/docker-for-windows-on-hyper-v-fix-the-host-volume-sharing-issue/)
 explains what to do.

### Environment variables ######
In the api folder under the root of your checkout create an environment file called ".env" with contents as described here:

```
DB_HOST=postgres
DB_PORT=5432
POSTGRES_PASSWORD=develop123
POSTGRES_USER=develop
POSTGRES_DB=notifikation
LOG_STD=OUT
LOG_STD_LEVEL=DEBUG
AUTHORIZATION_TOKEN=<some token>
```

### Docker Compose usage #####
When using e.g. Windows PowerShell, change directory to the root of the phpstorm project, this is where the 
docker-compose.yml should reside. To start the docker containers execute:
```
docker-compose up -d
```
If everything went well, you should then be able to run:
```
docker ps -a
```
And you should see something like the following output:
```
CONTAINER ID        IMAGE               COMMAND                  CREATED             STATUS                   PORTS                    NAMES
d6b5dc686c2b        notifikation_php    "docker-php-entrypoi…"   6 hours ago         Up 6 hours               0.0.0.0:80->80/tcp       notifikation_php
803c484a7743        postgres            "docker-entrypoint.s…"   6 hours ago         Up 6 hours               0.0.0.0:5432->5432/tcp   notifikation_postgres
```

In phpstorm click "Open edit Run/Debug configurations dialog", click the + sign
and choose Docker->Docker Compose, a dialog opens where you have to point it on the 
.yml file and call the command something like "Start Servers", to always be up to
date check
>"--build, force build images".

Now the servers can be run from phpstorm. You can test it first running the following from your 
shell:
```
docker-compose down
```
And then press the "Run" button for your run configuration in phpstorm.

### Database setup ######
Docker did create the database specified in the .env file for us, and it has been automatically provisioned from the entity.sql file in the project root. 
You should be able to connect to the database from phpstorm on localhost with the credentials supplied by your .env file.

After the database is deployed, make sure to add changes to that file in a manner so that it safely runs against a running database, e.g. useing ALTER TABLE, or ALTER TYPE, etc.

### Installing dependencies with composer ######
Open up Windows Powershell and navigate to where the docker-compose.yml resides, run composer install as follows:
```
docker run -it --rm  --mount type=bind,src="$PWD",target=/var/www -w /var/www/api composer install
```
This runs composer in a throw away container and installs our dependencies. Likewise it is possible to require new dependencies by replacing "composer install" with e.g. "composer require phpunit/phpunit --dev".

### Set up testing ######
In order to execute and write codeception api tests, you must first configure phpstorm to be able to open up a terminal to your php container. 
This can be done by opening phpstorm Settings -> Tools -> Terminal, and adding the following shellpath:

```
"cmd.exe" /k ""C:\Program Files\Docker\Docker\resources\bin\docker-compose.exe" exec php bash"
``` 

Open the terminal by hitting ALT+F12 on your keyboard. You should now have a prompt to your working dir /var/www/api. Type the following to execute all tests:

```
php vendor/bin/codecept run
```

To run a specific test do e.g.:

```
php vendor/bin/codecept run tests/CreateNotifikationCest.php
```

To generate a new test file do e.g.:

```
php vendor/bin/codecept generate:cest api SomeEndpointCest.php
```

You can read more about codeception testing [here](https://codeception.com/docs/modules/REST)

#### Create test data ####
Typically you will need a create endpoint first, so that you can create testdata for e.g. update, list and read operations.
The procedure is to use postman to create testdata in the database for you. 
Then using phpstorm database connection feature, right click the connection and choose "Dump Data to File" -> "SQL Inserts".
This will create a file locally, copy the desired insert statements from here to your ./api/tests/_data/testData.sql file, 
which will be run for each of the tests executed.

### Configure Xdebug ######
In order to get xdebug to work with phpstorm we need to do 2 things.

1. Configure remote interpreter
Edit your settings in phpstorm, under "Languages & Frameworks" -> "php" -> and configure CLI
interpreter, choose docker-compose

2. Configure a run configuration
Add a new Docker->Docker-Compose configuration, point out the docker-compose.yml file
, give it a saying name like "Start Servers" and we are ready to go.

>Set server to "localhost" and idekey to "PHPSTORM"

#### Testing that debugging works: #####
Set a breakpoint in your code, click the button "Start Listening for PHP Debug Connections"
next to the run configuration. Phpstorm should now automatically detect incoming debug session. 

# PHPStorm #
The docker start/play button does not start the containers.
From terminal run: 
>docker-compose up

